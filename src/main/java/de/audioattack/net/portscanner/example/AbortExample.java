package de.audioattack.net.portscanner.example;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.UUID;

import de.audioattack.net.portscanner.PortScanner;
import de.audioattack.net.portscanner.dictionary.IPortDictionary;
import de.audioattack.net.portscanner.dictionary.ProtocolInfo;
import de.audioattack.net.portscanner.dictionary.tcp.IanaTcpDictionary;
import de.audioattack.net.portscanner.listener.IScannerStatusListener;
import de.audioattack.net.portscanner.scan.enums.PortResult;
import de.audioattack.net.portscanner.scan.enums.TransportProtocol;

public class AbortExample {

  private UUID uuid;

  public void startAndAbort() {

    final PortScanner scanner = new PortScanner(new IScannerStatusListener() {

      final IPortDictionary ports = new IanaTcpDictionary();

      public void onScanStarted(UUID jobId) {
        AbortExample.this.uuid = jobId;
        System.out.println("Scan started: " + jobId);
      }

      public void onScanFinished(final UUID jobId) {
        System.out.println("Scan finished.");
      }

      public void onScanAborted(final UUID jobId) {
        System.out.println("Scan aborted.");
      }

      public void onHostFound(final UUID jobId, final String host) {
        System.out.println("Host: " + host);
      }

      @Override
      public void onPortResult(final UUID jobId, final String host, final int port, final PortResult result,
          final TransportProtocol protocol) {
        System.out
            .println("Host: " + host + " " + protocol + " port: " + port + " " + getNames(ports.getProtocolInfo(port)));
      }
    });

    new Thread(new Runnable() {

      public void run() {

        try {
          scanner.discover(null, InetAddress.getByName("localhost"), (short) 8, true, false, true);
        } catch (UnknownHostException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }
    }).start();

    while (null == uuid) {
      System.out.println(uuid);
      try {
        Thread.sleep(50);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

    scanner.abort(uuid);

  }

  private static String getNames(final List<ProtocolInfo> infos) {

    final StringBuilder sb = new StringBuilder();

    for (final ProtocolInfo info : infos) {
      if (sb.length() > 0) {
        sb.append(", ");
      }
      sb.append(info.getName());
    }

    return sb.toString();
  }

  public static void main(final String... args) {

    new AbortExample().startAndAbort();
  }

}
