package de.audioattack.net.portscanner.example;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.List;
import java.util.UUID;

import de.audioattack.net.portscanner.PortScanner;
import de.audioattack.net.portscanner.dictionary.IPortDictionary;
import de.audioattack.net.portscanner.dictionary.ProtocolInfo;
import de.audioattack.net.portscanner.dictionary.tcp.IanaTcpDictionary;
import de.audioattack.net.portscanner.dictionary.udp.IanaUdpDictionary;
import de.audioattack.net.portscanner.iterator.DictionaryPortIterator;
import de.audioattack.net.portscanner.iterator.PortRange;
import de.audioattack.net.portscanner.iterator.PortRangeIterator;
import de.audioattack.net.portscanner.iterator.SinglePortIterator;
import de.audioattack.net.portscanner.listener.IScannerStatusListener;
import de.audioattack.net.portscanner.scan.enums.PortResult;
import de.audioattack.net.portscanner.scan.enums.TransportProtocol;

public class PortscannerExample {

  /**
   * Used for testing. Ignore it if you don't need it.
   *
   * @param args
   *          will be ignored
   * @exception UnknownHostException
   *              if IP address is of illegal length
   */
  public static void main(final String[] args) throws UnknownHostException {

    final long start = System.currentTimeMillis();

    final IPortDictionary tcpPorts = new IanaTcpDictionary();
    final IPortDictionary udpPorts = new IanaUdpDictionary();

    final PortScanner scanner = new PortScanner(new IScannerStatusListener() {

      public void onScanStarted(final UUID jobId) {
        System.out.println("Scan started: " + jobId);
      }

      public void onScanFinished(final UUID jobId) {
        System.out.println("Scan finished: " + jobId);
      }

      public void onScanAborted(final UUID jobId) {
        System.out.println("Scan aborted: " + jobId);
      }

      public void onHostFound(final UUID jobId, final String host) {
        System.out.println("Host: " + host);
      }

      @Override
      public void onPortResult(final UUID jobId, final String host, final int port, final PortResult result,
          final TransportProtocol protocol) {
        if (result != PortResult.CLOSED) {
          System.out.println("Host: " + host + " "
              + protocol + " port: " + port + " " + (protocol == TransportProtocol.TCP
                  ? getNames(tcpPorts.getProtocolInfo(port)) : getNames(udpPorts.getProtocolInfo(port)))
              + " " + result);
        }
      }
    });

    try {
      Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();

      while (networkInterfaces.hasMoreElements()) {
        NetworkInterface networkInterface = (NetworkInterface) networkInterfaces.nextElement();

        if (networkInterface.isUp()) {

          System.out.println("Scanning interface " + networkInterface.getName());

          for (final InterfaceAddress interfaceAddress : networkInterface.getInterfaceAddresses()) {

            if (interfaceAddress.getAddress().isLoopbackAddress()) {
              System.out.println("Skipping loopback address " + interfaceAddress.getAddress() + ".");
            } else if (interfaceAddress.getAddress() instanceof Inet6Address
                && interfaceAddress.getNetworkPrefixLength() < 120) {
              System.out.println(
                  "Skipping IPv6 address " + interfaceAddress.getAddress() + ", because prefix length is too low: "
                      + interfaceAddress.getNetworkPrefixLength() + ". We don't want to wait until who knows when...");
            } else {
              System.out.println(
                  "Scannning " + interfaceAddress.getAddress() + "/" + interfaceAddress.getNetworkPrefixLength());
              scanner.discover(null, interfaceAddress.getAddress(), interfaceAddress.getNetworkPrefixLength(), true,
                  true, true);
            }
          }
        } else {

          System.out.println(networkInterface.getName() + " is down");
        }
      }

    } catch (SocketException e) {
      e.printStackTrace();
    }

    scanner.scan(null, TransportProtocol.UDP, InetAddress.getByName("192.168.1.1"),
        new PortRangeIterator(new PortRange(50, 60)));

    scanner.scan(null, TransportProtocol.TCP, InetAddress.getByName("allesehersonerdshier.net"),
        new SinglePortIterator(80, 443));

    scanner.scan(null, TransportProtocol.TCP, InetAddress.getByName("::1"), new DictionaryPortIterator(tcpPorts));
    System.out.println("time: " + (System.currentTimeMillis() - start) + " ms");
  }

  private static String getNames(final List<ProtocolInfo> infos) {

    if (infos == null) {
      return "";
    }

    final StringBuilder sb = new StringBuilder();

    for (final ProtocolInfo info : infos) {
      if (sb.length() > 0) {
        sb.append(", ");
      }
      sb.append(info.getName());
    }

    return sb.toString();
  }
}
